fbconnect_bridge provides a set of modules that brigde the gap between FBConnect and various Drupal modules.
This approach enables both the original contributed module to be independent of each other, while giving the
much needed integration between the contributed module and Facebook.

fbconnect_bridge itself is not a module, enable one of the modules under this based on the integration you 
need. 
