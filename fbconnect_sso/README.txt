ABOUT

The module provides the necessary integration between the fbconnect module and SSO module
so that multi-sites that use SSO module can use fbconnect module for logging on with 
fb credentials.

INSTALLATION

1. Deploy the fbconnect_sso module and enable it on both sso controller and sso client sites.
2. Table fbconnect_users table must be shared between the controller and client sites  


CONFIGURATION

Fbconnect module must be installed on both the SSO Controller and SSO Client sites and must 
be configured with the same appkey and secret key
