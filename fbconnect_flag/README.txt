---SUMMARY---
Module provides the capability to publish a node to FB user's feed when the user select a flag

--- REQUIREMENTS ---
The FBConnect module (http://drupal.org/project/fbconnect) must be installed and configured.

---INSTALLATION---
1. Extract and enable fbconnect_flag module
2. Create / edit a Flag (Administer -> Site Building -> Flag). 
   2.1 Use Normal links for creating flags.
   2.2 Select "Publish to user's Facebook feed" under Display option
   2.3 Save
